import App from 'next/app';
import Head from 'next/head';
import { DataTable, TextStyle, Avatar, ResourceList, Card, Page, AppProvider } from '@shopify/polaris';
import { Provider } from '@shopify/app-bridge-react';
import Cookies from "js-cookie";
import '@shopify/polaris/styles.css';
// import {showTable} from './dataTable';
import VerbPicker from './dataTable';

class MyApp extends App {
  
  render() {
    const { Component, pageProps } = this.props;
    const config = { apiKey: API_KEY, shopOrigin: Cookies.get("shopOrigin"), forceRedirect: true };
    return ( 
      <AppProvider
        i18n={{
          Polaris: {
            ResourceList: {
              sortingLabel: 'Sort by',
              defaultItemSingular: 'item',
              defaultItemPlural: 'items',
              showing: 'Showing {itemsCount} {resource}',
              Item: {
                viewItem: 'View details for {itemName}',
              },
            },
            Common: {
              checkbox: 'checkbox',
            },
          },
        }}
      >        
        <Page>
          <VerbPicker></VerbPicker>
          <Card>
            <ResourceList
              showHeader
              items={[
                {
                  id: 341,
                  url: 'customers/341',
                  name: 'Mae Jemison',
                  location: 'Decatur, USA',
                },
                {
                  id: 256,
                  url: 'customers/256',
                  name: 'Ellen Ochoa',
                  location: 'Los Angeles, USA',
                },
              ]}
              renderItem={(item) => {
                const {id, url, name, location} = item;
                const media = <Avatar customer size="medium" name={name} />;

                return (
                  <ResourceList.Item id={id} url={url} media={media}>
                    <h3>
                      <TextStyle variation="strong">{name}</TextStyle>
                    </h3>
                    <div>{location}</div>
                  </ResourceList.Item>
                );
              }}
            />
          </Card>
        </Page>
</AppProvider>
    );
  }
}

// function showTable() {
//   const rows = [
//     ['Emerald Silk Gown', '$875.00', 124689, 140, '$122,500.00'],
//     ['Mauve Cashmere Scarf', '$230.00', 124533, 83, '$19,090.00'],
//     [
//       'Navy Merino Wool Blazer with khaki chinos and yellow belt',
//       '$445.00',
//       124518,
//       32,
//       '$14,240.00',
//     ],
//   ];

//   return (
//       <Card>
//         <DataTable
//           columnContentTypes={[
//             'text',
//             'numeric',
//             'numeric',
//             'numeric',
//             'numeric',
//           ]}
//           headings={[
//             'Product',
//             'Price',
//             'SKU Number',
//             'Net quantity',
//             'Net sales',
//           ]}
//           rows={rows}
//           totals={['', '', '', 255, '$155,830.00']}
//         />
//       </Card>
//   );
// }

export default MyApp;
